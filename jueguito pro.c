#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <conio.h>
#include <windows.h>
#include "biblio de pros.h"
#define COL 10
#define REN 10

int main()
{
    char tablero_compu[REN][COL], barcos_compu[REN][COL], tablero_user[REN][COL];
    int opc=0, ganador;
    srand(time(NULL));
    do
    {
        opc=menu();
        switch(opc)
        {
        case 0:
            system("cls");
            inicializa_tablero(tablero_user);
            obtenerBarcosUser(tablero_user);
            inicializa_tablero(barcos_compu);
            inicializa_tablero(tablero_compu);
            inicializa_objetivosCompu(barcos_compu);
            //imprime_tablero(barcos_compu, 1);
            //imprime_tablero(tablero_user, 0);
            ganador = juego(tablero_user, tablero_compu, barcos_compu);
            if(ganador==1)
            {
                printf("VICTORIA!!!");
            }
            else
            {
                printf("DIME, �QUE SE SIENTE QUE ALGO QUE NI SIQUIERA SE CONSIDERA SER VIVO TE GANE?");
            }
            Sleep(15000);
            break;
        case 1:
            system("cls");
            instrucciones();
            break;
        }
    }
    while(opc!=2);
    return 0;
}
